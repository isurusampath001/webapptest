USE [master]
GO
/****** Object:  Database [db_test]    Script Date: 9/25/2021 4:39:47 PM ******/
CREATE DATABASE [db_test]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_test', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\db_test.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'db_test_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\db_test_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [db_test] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_test].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_test] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_test] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_test] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_test] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_test] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_test] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_test] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_test] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_test] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_test] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_test] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_test] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_test] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_test] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_test] SET  DISABLE_BROKER 
GO
ALTER DATABASE [db_test] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_test] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_test] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_test] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_test] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_test] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_test] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_test] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [db_test] SET  MULTI_USER 
GO
ALTER DATABASE [db_test] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_test] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_test] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_test] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [db_test] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [db_test] SET QUERY_STORE = OFF
GO
USE [db_test]
GO
/****** Object:  Table [dbo].[SLTStaff]    Script Date: 9/25/2021 4:39:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SLTStaff](
	[emp_id] [varchar](10) NOT NULL,
	[name] [varchar](255) NULL,
	[is_active] [int] NOT NULL,
 CONSTRAINT [PK_SLTStaff] PRIMARY KEY CLUSTERED 
(
	[emp_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[SLTStaff] ([emp_id], [name], [is_active]) VALUES (N'015750', N'sarawana', 1)
INSERT [dbo].[SLTStaff] ([emp_id], [name], [is_active]) VALUES (N'015751', N'isuru', 1)
INSERT [dbo].[SLTStaff] ([emp_id], [name], [is_active]) VALUES (N'015752', N'pubudu', 1)
INSERT [dbo].[SLTStaff] ([emp_id], [name], [is_active]) VALUES (N'015753', N'piyumal', 1)
INSERT [dbo].[SLTStaff] ([emp_id], [name], [is_active]) VALUES (N'015754', N'nelumi', 0)
GO
USE [master]
GO
ALTER DATABASE [db_test] SET  READ_WRITE 
GO
