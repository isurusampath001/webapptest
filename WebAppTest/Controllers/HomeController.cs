﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppTest.Models;

namespace WebAppTest.Controllers
{
    public class HomeController : Controller
    {

        db_testEntities context = new db_testEntities();

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult GetEmployeeDetails()
        {
            var list = context.SLTStaffs.SqlQuery("SELECT * FROM SLTStaff WHERE is_active = 1");
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult CreateEmployee(SLTStaff sLTStaff)
        {
            context.SLTStaffs.Add(sLTStaff);
            string msg ;
            try
            {
                context.SaveChanges();
                msg = "Success";
            }
            catch (Exception ex)
            {
                msg = "Error: " + ex;
            }


            return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
        }


        [HttpPost]
        public ActionResult UpdateEmployee(SLTStaffView sLTStaffView)
        {
            if(sLTStaffView != null)
            {
                SLTStaff obj = sLTStaffView.sltstaff;
                string query = "UPDATE SLTStaff SET emp_id = '"+obj.emp_id+"',name = '"+obj.name+"' WHERE emp_id = '"+ sLTStaffView .editEmployeeId+ "'";
                string msg;
                try
                {
                    var v = context.Database.ExecuteSqlCommand(query);
                    msg = "Success"; 
                }
                catch (Exception ex)
                {
                    msg = "Error: "+ex ;
                }

                return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
            }
            else
            {
                return null;
            }
        }


        [HttpPost]
        public ActionResult DeleteEmployee(string deleteId)
        {
            if (deleteId != null)
            {
                string query = "UPDATE SLTStaff SET is_active = 0 WHERE emp_id = '" + deleteId + "'";
                string msg;
                try
                {
                    var v = context.Database.ExecuteSqlCommand(query);
                    msg = "Success";
                }
                catch (Exception ex)
                {
                    msg = "Error: " + ex;
                }

                return Json(new { Message = msg, JsonRequestBehavior.AllowGet });
            }
            else
            {
                return null;
            }
        }

    }
}